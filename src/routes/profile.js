const express = require("express");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const { updateProfile, getProfileById } = require("../controllers/profilecontroller");
const profileRouter = express.Router();

profileRouter.get("/getprofile", async(request,response) => {
  try {
    const result = await getProfileById(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

profileRouter.post("/updateprofile", async (request,response) => {
  try {
    const result = await updateProfile(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

module.exports = profileRouter;
