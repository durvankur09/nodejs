
const express = require("express");
const { getDestricts, getSearchData } = require("../controllers/districtscontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const districtRouter = express.Router();

districtRouter.get("/districts/:value", async(request,response) => {
  try {
    const value = request.params.value ? request.params.value == "null" ? null : request.params.value : null; 
    const result = await getDestricts(value);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

districtRouter.get("/getsearchdata/:searchtypeid/:value", async(request,response) => {
  try {
    const result = await getSearchData(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

module.exports = districtRouter;
