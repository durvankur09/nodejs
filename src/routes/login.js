const express = require("express");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const { addUser, loginUser, resetToken } = require("../controllers/logincontroller");
const loginRouter = express.Router();

loginRouter.post("/signup", async(request,response) => {
  try {
    const result = await addUser(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.signup_fail);
  }
});

loginRouter.post("/login", async (request,response) => {
  try {
    const result = await loginUser(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.login_fail);
  }
});

loginRouter.get("/refreshtoken", async (request,response) => {
  try {
    const result = await resetToken(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.login_fail);
  }
});

module.exports = loginRouter;
