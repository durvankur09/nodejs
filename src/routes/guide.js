
const express = require("express");
const { getGuides } = require("../controllers/guidecontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const guideRouter = express.Router();

guideRouter.get("/guidelist/:fortid", async(request,response) => {
    try {
      const fortid = request.params.fortid ? request.params.fortid == "null" ? null : request.params.fortid : null; 
      const result = await getGuides(fortid);
      return sendResponse(response, result);
    } catch(err) {
      return sendError(response, err.sqlMessage || messages.something_went_wrong);
    }
  });

module.exports = guideRouter;
