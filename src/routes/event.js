
const express = require("express");
const { createTrekkerPlan, createEvent, getEvents, getEventById, updateParticipantStatus, creatParticipants, updateRating } = require("../controllers/eventcontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const eventrouter = express.Router();

eventrouter.post("/createtrekkerplan", async(request,response) => {
  try {
    const result = await createTrekkerPlan(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.post("/createevent", async(request,response) => {
  try {
    const result = await createEvent(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.post("/events", async(request,response) => {
  try {
    const result = await getEvents(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.get("/getevent/:eventid", async(request,response) => {
  try {
    const result = await getEventById(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.get("/eventaction/:eventid/:participantid/:participantstatusid/:actionid", async(request,response) => {
  try {
    const result = await updateParticipantStatus(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.get("/applyevent/:eventid/:participantid", async(request,response) => {
  try {
    const result = await creatParticipants(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

eventrouter.post("/updaterating", async(request,response) => {
  try {
    const result = await updateRating(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});


module.exports = eventrouter;
