
const express = require("express");
const { getEventsByOrganiser } = require("../controllers/eventcontroller");
const { getReviewsByevents } = require("../controllers/reviewcontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const reviewrouter = express.Router();

reviewrouter.get("/geteventbyorganiser", async(request,response) => {
  try {
    const result = await getEventsByOrganiser(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

reviewrouter.get("/getratingsbyevent/:eventid", async(request,response) => {
  try {
    const result = await getReviewsByevents(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

module.exports = reviewrouter;
