
const express = require("express");
const { getForts } = require("../controllers/fortcontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const fortRouter = express.Router();

fortRouter.get("/forts/:value", async(request,response) => {
  try {
    const value = request.params.value ? request.params.value == "null" ? null : request.params.value : null; 
    const result = await getForts(value);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

module.exports = fortRouter;
