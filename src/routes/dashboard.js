
const express = require("express");
const { getDashboardData } = require("../controllers/dashboardcontroller");
const { sendError, sendResponse } = require("../utility/index");
const messages = require("../constants/sendmessages");
const dashboardrouter = express.Router();

dashboardrouter.get("/geteventsoftrekker", async(request,response) => {
  try {
    const result = await getEventByTrekker(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

dashboardrouter.get("/organiserdashboard", async(request,response) => {
  try {
    const result = await getDashboardData(request);
    return sendResponse(response, result);
  } catch(err) {
    return sendError(response, err.sqlMessage || messages.something_went_wrong);
  }
});

module.exports = dashboardrouter;
