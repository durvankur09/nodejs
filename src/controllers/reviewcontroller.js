
const {  getRatingsOfEvent } = require("../models/review");
const { sendResult, verifyToken } = require("../utility/index");
const messages = require("../constants/sendmessages");

const getReviewsByevents = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
      eventid: !request.params.eventid || request.params.eventid == 'null' ? null : request.params.eventid,
    };
    if (event.organiserid && event.eventid) {
      const result = await getRatingsOfEvent(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.user_notexist);
    }
  } catch (error) {
    throw error;
  }  
};

module.exports = {
  getReviewsByevents,
};
