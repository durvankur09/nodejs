const { addNewUser, getOrganiserByEmail, getTrekkerByEmail, setLogin } = require("../models/login");
const { getEncryptedData, compareEncrytedData, sendResult, encodeToken, verifyToken, getDateFormatToStore } = require("../utility/index");
const messages = require("../constants/sendmessages");
const { moment } = require("../utility/librarydata");

const addUser = async (request)  => {
  const user = {
    "roleid": request.body.roleid,
    "email": request.body.email,
    "password": getEncryptedData(request.body.password),
    "other_logindata": request.body.other_logindata,
    "username": request.body.username,
    image: request.body.image,
  };
  const { roleid, email, password, username } = user;
  try {
    if (roleid && email && password && username) {
      const organiser = await getOrganiserByEmail(user.email);
      const trekker = await getTrekkerByEmail(user.email);
      const existuser = organiser || trekker;
      // check a email id exist or not if yes send error as true as first parameter
      if (existuser) {
        return sendResult(true, null, messages.user_exist);
      } else {
        // if email id is not exist then add a new user into database and send a result object and error is to null
        const result = await addNewUser(user);
        return sendResult(false, result, null);
      }
    } else {
      return sendResult(true, null, messages.please_fill_all_fields);
    }
  } catch (error) {
    throw error;
  }   
};

const loginUser = async (request)  => {
  const user = {
    "email": request.body.email,
    "password": getEncryptedData(request.body.password),
    "other_logindata": request.body.other_logindata,
  };
  const { email, password } = user;
  try {
    if (email && password) {
      const organiser = await getOrganiserByEmail(user.email);
      const trekker = await getTrekkerByEmail(user.email);
      const result = organiser || trekker;
      const finalobject = {
        userid: result.organiserid || result.trekkerid,
        username: result.organiser_name || result.trekkername,
        email: result.email,
        image: result.image,
        roleid: result.organiserid ? 1 : 2,
        expirytime: Math.floor(Date.now() / 1000) + parseInt(process.env.JWT_EXPIRY_TIME)
      };
      finalobject.token = encodeToken(finalobject);
      const loginobject = {
        userid: result.organiserid || result.trekkerid,
        username: result.organiser_name || result.trekkername,
        password: user.password,
        email: result.email,
        roleid: result.organiserid ? 1 : 2,
        other_logindata: null,
        logintypeid: 99,
        date: getDateFormatToStore(),
      };
      const loginsuccess = await setLogin(loginobject);
      if (result && loginsuccess) {
        if (compareEncrytedData(request.body.password, result.password))
          return sendResult(false, finalobject, null);
        else 
          return sendResult(true, null, messages.wrong_data);
      } else {
        return sendResult(true, null, messages.record_no_found);
      }
    } else {
      return sendResult(true, null, messages.please_fill_all_fields);
    }
  } catch (error) {
    throw error;
  }   
};

const resetToken = async (request) => {
  try {
    const user = verifyToken(request.headers.authorization);
    const organiser = await getOrganiserByEmail(user.email);
    const trekker = await getTrekkerByEmail(user.email);
    const result = organiser || trekker;
    const finalobject = {
      userid: result.organiserid || result.trekkerid,
      username: result.organiser_name || result.trekkername,
      email: result.email,
      image: result.image,
      expirytime: Math.floor(Date.now() / 1000) + parseInt(process.env.JWT_EXPIRY_TIME)
    };

    // here just before expiring token changing with new token as result
    finalobject.token = encodeToken(finalobject);
    if (result && finalobject) {
      return sendResult(false, finalobject, null);
    } else {
      return sendResult(true, null, messages.record_no_found);
    }
  } catch (error) {
    throw error;
  } 
}

module.exports = {
  addUser,
  loginUser,
  resetToken,
};
