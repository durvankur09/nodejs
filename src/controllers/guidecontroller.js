
const { getAllGuides } = require("../models/guide");
const { sendResult } = require("../utility/index");

const getGuides = async(fortid)  => {
    try {
        const result = await getAllGuides(fortid);
        return sendResult(false, result, null);
    } catch (error) {
        throw error;
    }   
};

module.exports = {
  getGuides,
};
