
const { getAllForts } = require("../models/fort");
const { sendResult } = require("../utility/index");

const getForts = async(value)  => {
    try {
        const result = await getAllForts(value);
        return sendResult(false, result, null);
    } catch (error) {
        throw error;
    }   
};

module.exports = {
  getForts,
};
