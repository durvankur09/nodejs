
const { getAllDestricts } = require("../models/district");
const { getAllForts } = require("../models/fort");
const { sendResult } = require("../utility/index");

const getDestricts = async(value)  => {
    try {
        const result = await getAllDestricts(value);
        return sendResult(false, result, null);
    } catch (error) {
        throw error;
    }   
};

const getSearchData = async(request) => {
    try {
        const searchtypeid = request.params.searchtypeid;
        const value = request.params.value ? request.params.value == "null" ? null : request.params.value : null; 
        const result = searchtypeid == 1 ? await getAllForts(value) : await getAllDestricts(value);
        return sendResult(false, result, null);
    } catch (error) {
        throw error;
    }   
};

module.exports = {
    getDestricts,
    getSearchData,
};
