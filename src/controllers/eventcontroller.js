
const { createNewTrekkerPlan, createventfort, createEventPlan, getALLEvents, getEventFromId, updatePartipantsStatus, aplyForevent, updateRatingOfEvent, getEventOfOrganiser, getEventOfTrekker } = require("../models/event");
const { sendResult, verifyToken } = require("../utility/index");
const { moment } = require("../utility/librarydata");
const messages = require("../constants/sendmessages");

const createTrekkerPlan = async(request) => {
  try {
    const obj = verifyToken(request.headers.authorization);
    const event = {
      eventid: request.body.eventid,
      eventname: request.body.eventname,
      forts: request.body.fortids,
      fromdate: request.body.fromdate,
      todate: request.body.todate,
      guideid: request.body.guideid || null,
      eventstatus: request.body.eventstatus || 2,
      trekkerid: obj ? obj.userid : null,
    };

    if (event.eventname && event.forts && event.fromdate && event.todate && event.trekkerid) {
      const forts = event.forts.split(",");
      const result = await createNewTrekkerPlan(event);
      console.log('result: ', result);
      const eventid = result.insertId || event.eventid;
      const tempresult = await createventfort({ eventid, forts });
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.please_fill_all_fields);
    }
  } catch (error) {
      throw error;
  }     
};

const createEvent = async(request) => {
  try {
    const obj = verifyToken(request.headers.authorization);
    const event = {
      eventid: request.body.eventid,
      eventname: request.body.eventname,
      forts: request.body.fortids,
      description: request.body.description,
      fromdate: request.body.fromdate,
      todate: request.body.todate,
      guideid: null,
      eventstatus: request.body.eventstatus || 2,
      organiserid: obj ? obj.userid : null,
      participant_count: request.body.participant_count,
      price: request.body.price,
      pickup_points: JSON.stringify(request.body.pickup_points),
      things_to_carry: JSON.stringify(request.body.things_to_carry),
    };

    if (event.eventname && event.forts && event.fromdate && event.todate && event.organiserid && event.description && event.participant_count && event.pickup_points && event.things_to_carry) {
      const forts = event.forts.split(",");
      const result = await createEventPlan(event);
      const eventid = result.insertId || event.eventid;
      const tempresult = await createventfort({ eventid, forts });
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.please_fill_all_fields);
    }
  } catch (error) {
    throw error;
  }
};

const getEvents = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      fromdate: request.body.fromdate,
      todate: request.body.todate,
      districtid: request.body.districtid || null,
      forts: request.body.fortids ? request.body.fortids.split(",") : null,
      gradeid: request.body.gradeid || null,
      page: parseInt(request.body.page) - 1,
      perpage: request.body.perpage,
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
    };
    const result = await getALLEvents(event);
    const startdate = moment(new Date()).startOf('month').format("YYYY-MM-DD HH:mm:ss");
    const enddate = moment(new Date()).endOf('month').format("YYYY-MM-DD HH:mm:ss");
    const finalresult = [
      {
        events: result.map((e) => ({
          ...e,
          pickup_points: e.pickup_points ? JSON.parse(e.pickup_points) : [],
          things_to_carry: e.things_to_carry ? JSON.parse(e.things_to_carry) : [],
        })),
        totalevents: result.length,
        gradewiseevents: [
          {
            gradeid: 1,
            gradename: "Easy",
            count: event.gradeid
              ? result.filter((res) => res.gradeid == event.gradeid).length
              : result.filter((res) => res.gradeid == 1).length,
          },
          {
            gradeid: 2,
            gradename: "Medium",
            count: event.gradeid
            ? result.filter((res) => res.gradeid == event.gradeid).length
            : result.filter((res) => res.gradeid == 2).length,
          },
          {
            gradeid: 3,
            gradename: "Tough",
            count: event.gradeid
            ? result.filter((res) => res.gradeid == event.gradeid).length
            : result.filter((res) => res.gradeid == 3).length,
          },
        ],
        districtwisevents: event.districtid ? result.filter((res) => res.districtid == event.districtid).length : 0,
        orgiserevents: event.organiserid ? result.length : 0,
        trekkerevents: event.trekkerid ? result.filter((res) => res.trekkerid == event.trekkerid).length : 0,
        currentmonthevents: result.filter((res) => res.fromdate >= startdate && res.fromdate <= enddate).length,
      },
    ];
    return sendResult(false, finalresult, null);
  } catch (error) {
    throw error;
  }   
};

const getEventById = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      eventid: request.params.eventid,
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
    };
    if (event.eventid && (event.organiserid || event.trekkerid)) {
      const result = await getEventFromId(event);
      return sendResult(false, result.map((e) => ({
        ...e,
        pickup_points: e.pickup_points ? JSON.parse(e.pickup_points) : [],
        things_to_carry: e.things_to_carry ? JSON.parse(e.things_to_carry) : [],
      })), null);
    } else {
      return sendResult(true, null, !event.eventid ? messages.event_notexist : !event.organiserid || !event.trekkerid ? messages.user_notexist : "");
    }
  } catch (error) {
    throw error;
  }   
};

const updateParticipantStatus = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      eventid: request.params.eventid,
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
      participantid: request.params.participantid,
      actionid: request.params.actionid,
      participantstatusid: request.params.participantstatusid,
    };
    if (event.eventid && (event.organiserid || event.trekkerid) && event.participantid && event.actionid && event.participantstatusid) {
      const result = await updatePartipantsStatus(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, !event.eventid
        ? messages.event_notexist 
        : !event.organiserid || !event.trekkerid
          ? messages.user_notexist 
          : !event.participantid
           ? messages.participant_required
           : !event.actionid
              ? messages.action_required
              : messages.participant_required );
    }
  } catch (error) {
    throw error;
  }   
};

const creatParticipants = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      eventid: request.params.eventid,
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
      participantid: request.params.participantid,
    };
    if (event.eventid && (event.organiserid || event.trekkerid) && event.participantid) {
      const result = await aplyForevent(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, !event.eventid
        ? messages.event_notexist 
        : !event.organiserid || !event.trekkerid
          ? messages.user_notexist 
          : !event.participantid
           ? messages.participant_required
           : messages.something_went_wrong);
    }
  } catch (error) {
    throw error;
  }  
};

const updateRating = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      eventid: request.body.eventid,
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
      ratingid: request.body.ratingid,
      rating: request.body.rating,
      description: request.body.description,
    };
    if (event.eventid && (event.trekkerid) && event.rating) {
      const result = await updateRatingOfEvent(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, !event.eventid
        ? messages.event_notexist 
        : !event.trekkerid
          ? messages.user_notexist 
          : !event.rating
           ? messages.rating_required
           : messages.something_went_wrong);
    }
  } catch (error) {
    throw error;
  }  
};

const getEventsByOrganiser = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
    };
    if (event.organiserid) {
      const result = await getEventOfOrganiser(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.user_notexist);
    }
  } catch (error) {
    throw error;
  }  
};

const getEventByTrekker = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const event = {
      trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
    };
    if (event.trekkerid) {
      const result = await getEventOfTrekker(event);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.user_notexist);
    }
  } catch (error) {
    throw error;
  }  
};

module.exports = {
  createTrekkerPlan,
  createEvent,
  getEvents,
  getEventById,
  updateParticipantStatus,
  creatParticipants,
  updateRating,
  getEventByTrekker,
  getEventsByOrganiser,
};
