const { getAllDashboardData } = require("../models/dashboard");
const { sendResult, verifyToken } = require("../utility/index");
const messages = require("../constants/sendmessages");

const getDashboardData = async(request) => {
  try {
    const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
    const org = {
      organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
    };
    if (org.organiserid) {
      const result = await getAllDashboardData(org);
      return sendResult(false, [result], null);
    } else {
      return sendResult(true, null, !org.organiserid ? messages.user_notexist : "");
    }
  } catch (error) {
    throw error;
  }   
};

module.exports = {
  getDashboardData,
};
