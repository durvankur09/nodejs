const { getProfile, updateProfileModel } = require("../models/profile");
const { getEncryptedData, sendResult, verifyToken } = require("../utility/index");
const messages = require("../constants/sendmessages");

const updateProfile = async (request)  => {
  const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
  const profile = {
    roleid: obj.roleid,
    trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
    organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
    trekkername: obj ? obj.roleid == 2 ? request.body.username : null : null,
    organiser_name: obj ? obj.roleid == 1 ? request.body.username : null : null,
    email: request.body.email,
    password: getEncryptedData(request.body.password),
    mobile: request.body.mobile,
    alternate_number: request.body.alternate_number,
    address: request.body.address,
    pincode: request.body.pincode,
    city: request.body.city,
    state: request.body.state,
    country: request.body.country,
    image: request.body.image,
  };
  const { roleid, trekkerid, organiserid, organiser_name, trekkername, email, password } = profile;
  try {
    if (roleid && (trekkerid || organiserid) && (trekkername || organiser_name) && email && password) {
      const result = await updateProfileModel(profile);
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.please_fill_all_fields);
    }
  } catch (error) {
    throw error;
  }   
};

const getProfileById = async (request)  => {
  const obj = !request.headers.authorization || request.headers.authorization == "Global" ? null : verifyToken(request.headers.authorization);
  const profile = {
    trekkerid: obj ? obj.roleid == 2 ? obj.userid : null : null,
    organiserid: obj ? obj.roleid == 1 ? obj.userid : null : null,
  };
  const { trekkerid, organiserid } = profile;
  try {
    if (trekkerid || organiserid) {
      const result = await getProfile(profile);
      result.password
      return sendResult(false, result, null);
    } else {
      return sendResult(true, null, messages.user_notexist);
    }
  } catch (error) {
    throw error;
  }   
};

module.exports = {
  updateProfile,
  getProfileById,
};
