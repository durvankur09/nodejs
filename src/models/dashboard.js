
const query = require("../database");
const { moment } = require("../utility/librarydata");
const { getRatingsOfEvent } = require("./review");

const getAllDashboardData = async(org) => {
  try {
    const today = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    const avgquery = `select avg(r.rating) as avgrating from rating r join event e on r.eventid = e.eventid where e.organiserid = ${org.organiserid}`;
    const avgrating = await query(avgquery);
    const participantsquery = `select e.eventid, e.eventname, count(e.eventid) as count from participants p join event e on e.eventid = p.eventid
     join trekker t on t.trekkerid = p.trekkerid where e.organiserid = ${org.organiserid} group by e.eventid`;
    const participants = await query(participantsquery);
    const highratingsquery = `select count(*) as highratingcount from rating r join event e on r.eventid = e.eventid where e.organiserid = ${org.organiserid} and rating = 5`;
    const highratings = await query(highratingsquery);
    const totalratingsquery = `select count(*) as totalratingcount from rating r join event e on r.eventid = e.eventid where e.organiserid = ${org.organiserid}`;
    const totalratings = await query(totalratingsquery);
    const latesteventdonequery = `select e.eventid, e.eventname from event e where e.organiserid = ${org.organiserid} and e.fromdate < '${today}' order by fromdate desc limit 1`;
    const lastevent = await query(latesteventdonequery);
    const totalevents = participants.length;
    const pendingquery = `select e.eventid, e.eventname, count(e.eventid) as count from event e join participants p on p.eventid = e.eventid where e.organiserid = ${org.organiserid} and p.participantstatus = 1 group by e.eventid`;
    const pendingbookingevents = await query(pendingquery);
    const confirmquery = `select e.eventid, e.eventname, count(e.eventid) as count from event e join participants p on p.eventid = e.eventid where e.organiserid = ${org.organiserid} and p.participantstatus = 2 group by e.eventid`;
    const confirmbookingevents = await query(confirmquery);
    const obj = {
      avgrating: avgrating[0] ? avgrating[0].avgrating : null,
      participants,
      highratingcount: highratings[0] ? highratings[0].highratingcount : null,
      totalratingcount: totalratings[0] ? totalratings[0].totalratingcount : null,
      totalevents,
      pendingbookingevents,
      confirmbookingevents,
      lasteventreviews: [],
    };
    if (lastevent[0]) {
      const lasteventreviews = await getRatingsOfEvent(lastevent[0]);
      obj.lasteventreviews = lasteventreviews;
    }
    return await obj;
  } catch (error) {
    throw error;
  } 
};

module.exports = {
  getAllDashboardData,
};
