
const query = require("../database");

const getAllDestricts = async(value)  => {
    try {
        const stmt = value
            ? `select districtname, districtid, (active + 0) as active from district where instr(districtname, '${value}') > 0 and active = 1`
            : `select districtname, districtid, (active + 0) as active from district where active = 1`;         
        const result = await query(stmt);
        return await result;
    } catch (error) {
        throw error;
    }  
};


module.exports = {
    getAllDestricts,
};