
const query = require("../database");

const getAllGuides = async(fortid)  => {
    try {
        const stmt = fortid
            ? `select *, (active + 0) as activestatu from guide g join fortguides fg on fg.guideid = g.guideid where fg.fortid = ${fortid}`
            : `select *, (active + 0) as activestatus from guide`;         
        const result = await query(stmt);
        return await result;
    } catch (error) {
        throw error;
    }  
};

module.exports = {
  getAllGuides,
};