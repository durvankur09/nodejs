
const query = require("../database");

const getRatingsOfEvent = async(obj) => {
  try {
    let stmt = `select r.ratingid, r.description, r.rating, t.trekkername, t.image, t.trekkerid, r.eventid from rating r join trekker t on t.trekkerid = r.trekkerid join event e on e.eventid = r.eventid where e.eventid = ${obj.eventid}`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

module.exports = {
  getRatingsOfEvent,
};
