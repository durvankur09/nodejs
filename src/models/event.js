
const query = require("../database");

const createNewTrekkerPlan = async(event) => {
  try {
    if (event.eventid) {
      const stmt = `update event set eventname='${event.eventname}', eventstatus='${event.eventstatus}', 
      guideid='${event.guideid}', fromdate='${event.fromdate}', todate='${event.todate}', active = 1, trekkerid=${event.trekkerid} where eventid = ${event.eventid}`;
      const result = await query(stmt);
      return await result;
    } else {
      const stmt=`insert into event (eventname, eventstatus, guideid, fromdate, todate, active, trekkerid) values (?,?,?,?,?,?,?)`;
      const result = await query(stmt, [event.eventname, event.eventstatus, event.guideid, event.fromdate, event.todate, 1, event.trekkerid]);
      return await result;
    }
  } catch (error) {
    throw error;
  }   
};

const createventfort = async(eventfort) => {
  try {
    let stmt = `insert into eventforts (eventid, fortid) values`;
    eventfort.forts && eventfort.forts.forEach((fortid, index) => {
      if (index + 1 == eventfort.forts.length) {
        stmt = `${stmt} (${eventfort.eventid}, ${fortid});`; 
      } else {
        stmt = `${stmt} (${eventfort.eventid}, ${fortid}),`;
      };
    });
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  }   
};

const createEventPlan = async(event) => {
  try {
    if (event.eventid) {
      const stmt = `update event set eventname='${event.eventname}', eventstatus='${event.eventstatus}', 
      guideid=${event.guideid}, fromdate='${event.fromdate}', todate='${event.todate}', active = 1, organiserid=${event.organiserid}, price=${event.price},
      participant_count=${event.participant_count}, pickup_points='${event.pickup_points}', things_to_carry='${event.things_to_carry}', description='${event.description}' where eventid = ${event.eventid}`;
      console.log('stmt: ', stmt);
      const result = await query(stmt);
      return await result;
    } else {
      const stmt=`insert into event (eventname, eventstatus, guideid, fromdate, todate, active, organiserid, price, participant_count, pickup_points, things_to_carry, description) values (?,?,?,?,?,?,?,?,?,?,?,?)`;
      const result = await query(stmt, [event.eventname, event.eventstatus, event.guideid, event.fromdate, event.todate, 1, event.organiserid, event.price, event.participant_count, event.pickup_points, event.things_to_carry, event.description]);
      return await result;
    }
  } catch (error) {
    throw error;
  }   
};

const getALLEvents = async(obj) => {
  try {
    let stmt = `select e.eventid, e.eventname, e.eventstatus, e.description, e.organiserid, e.trekkerid,
    e.guideid, e.price, e.pickup_points, e.things_to_carry, e.participant_count, f.fortid, f.fortname, 
    f.rating, f.gradeid, f.districtid, f.image, f.basevillage, f.lattitude, f.longitude, (e.active + 0) as active,
    DATE_FORMAT(fromdate, "%Y-%m-%d %H:%i:%s") as fromdate, DATE_FORMAT(todate, "%Y-%m-%d %H:%i:%s") as todate 
    from event e join eventforts ef on ef.eventid = e.eventid join fort f on f.fortid = ef.fortid 
    where districtid = ifnull(${obj.districtid}, f.districtid) and gradeid = ifnull(${obj.gradeid}, f.gradeid)`;
    stmt = obj.organiserid ? `${stmt} and organiserid = ifnull(${obj.organiserid}, e.organiserid)` : `${stmt}`;
    stmt = obj.trekkerid ? `${stmt} and trekkerid = ifnull(${obj.trekkerid}, e.trekkerid)` : `${stmt}`;
    stmt = obj.fromdate ? `${stmt} and (e.fromdate between '${obj.fromdate}' and '${obj.todate}')` : `${stmt}`;
    obj.forts && obj.forts.length > 0 && obj.forts.forEach((fortid, index) => {
      if (obj.forts.length == 1) {
        stmt = `${stmt} and (ef.fortid like '${fortid}') group by e.eventid limit ${obj.perpage} offset ${obj.page};`;
      } else if (obj.forts.length != 1) {
        if (index == 0) {
          stmt = `${stmt} and (ef.fortid like '${fortid}'`;
        } else if (index + 1 == obj.forts.length) {
          stmt = `${stmt} or ef.fortid like '${fortid}') group by e.eventid limit ${obj.perpage} offset ${obj.page};`; 
        } else {
          stmt = `${stmt} or ef.fortid like '${fortid}'`;
        }
      }
    });
    stmt = !obj.forts ? `${stmt} group by e.eventid limit ${obj.perpage} offset ${obj.page};` : stmt;
    console.log('stmt: ', stmt);
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

const getEventOfOrganiser = async(obj) => {
  try {
    let stmt = `select e.eventid, e.eventname, e.eventstatus, e.organiserid, e.trekkerid,
    (e.active + 0) as active,
    DATE_FORMAT(fromdate, "%Y-%m-%d %H:%i:%s") as fromdate, DATE_FORMAT(todate, "%Y-%m-%d %H:%i:%s") as todate 
    from event e where organiserid=${obj.organiserid}`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

const getEventOfTrekker = async(obj) => {
  try {
    let stmt = `select e.eventid, e.eventname, e.eventstatus, e.organiserid, e.trekkerid,
    (e.active + 0) as active,
    DATE_FORMAT(fromdate, "%Y-%m-%d %H:%i:%s") as fromdate, DATE_FORMAT(todate, "%Y-%m-%d %H:%i:%s") as todate 
    from event e where trekkerid=${obj.trekkerid}`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

const getEventFromId = async(obj) => {
  try {
    const eventquery = `select e.eventid, e.eventname, e.eventstatus, e.description, e.organiserid, e.trekkerid,
      e.guideid, e.price, e.pickup_points, e.things_to_carry, e.participant_count, (e.active + 0) as active,
      DATE_FORMAT(fromdate, "%Y-%m-%d %H:%i:%s") as fromdate, DATE_FORMAT(todate, "%Y-%m-%d %H:%i:%s") as todate 
      from event e where eventid = ${obj.eventid}`;
    const event = await query(eventquery);
    const fortquery = `select f.fortid, f.fortname, f.gradeid, f.districtid, f.image, f.basevillage, f.rating from fort f 
    join eventforts ef on ef.fortid = f.fortid join event e on e.eventid = ef.eventid where e.eventid = ${obj.eventid}`;
    if (event[0]) {
      event[0].forts = await query(fortquery);
      if (event && event.organiserid) {
        const organiserquery = `select o.organiserid, o.organiser_name from organiser o where o.organiserid = ${event.organiserid}`;
        const organiser = await query(organiserquery);
        event[0].organiser = organiser[0] ? organiser[0] : null;
      }
      if (obj.organiserid) {
        const participantquery = `select t.trekkerid as participantid, t.trekkername as participantname, 
        (p.participantstatus + 0) as participantstatus, 
        p.participantid as participantstatusid from participants p join event e on e.eventid = p.eventid 
        join trekker t on t.trekkerid = p.trekkerid where e.eventid = ${obj.eventid}`;
        event[0].participants = await query(participantquery);
        const ratingquery = `select avg(r.rating) as avgrating from event e join rating r on r.eventid = e.eventid 
        where e.eventid = ${obj.eventid}`;
        const rating = await query(ratingquery);
        event[0].rating = rating[0] ? rating[0].avgrating : null;
      } else {
        const ratingquery = `select ratingid, rating, r.description as ratingdescription from rating r 
        join event e on r.eventid = e.eventid where r.trekkerid = ${obj.trekkerid}`;
        const rating = await query(ratingquery);
        event[0].rating = rating[0] ? rating[0].rating : null;
        event[0].ratingid = rating[0] ? rating[0].ratingid : null;
        event[0].ratingdescription = rating[0] ? rating[0].ratingdescription : null;
        event[0].participants = [];
      }
      return await event;
    }
  } catch (error) {
    throw error;
  } 
};

const updatePartipantsStatus = async(obj) => {
  try {
    const stmt = `update participants set trekkerid='${obj.participantid}', eventid='${obj.eventid}', 
    participantstatus='${obj.actionid}', active = 1 where participantid = '${obj.participantstatusid}'`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

const aplyForevent = async(obj) => {
  try {
    const stmt = `insert into participants (trekkerid, eventid, participantstatus, active) values (${obj.participantid},
       ${obj.eventid}, 1, 1)`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  } 
};

const updateRatingOfEvent = async(obj) => {
  try {
    if (obj.ratingid) {
      const stmt = `update rating set trekkerid='${obj.trekkerid}', eventid='${obj.eventid}', 
      description='${obj.description}', rating= '${obj.rating}', active = 1 where ratingid = ${obj.ratingid}`;
      const result = await query(stmt);
      return await result;
    } else {
      const stmt = `insert into rating (trekkerid, eventid, description, active, rating) values 
      (${obj.trekkerid}, ${obj.eventid}, '${obj.description}', 1, ${obj.rating})`;
      const result = await query(stmt);
      return await result;
    }
  } catch (error) {
    throw error;
  } 
};

module.exports = {
  createNewTrekkerPlan,
  createventfort,
  createEventPlan,
  getALLEvents,
  getEventFromId,
  updatePartipantsStatus,
  aplyForevent,
  updateRatingOfEvent,
  getEventOfOrganiser,
  getEventOfTrekker,
};
