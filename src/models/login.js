const query = require("../database");

const addNewUser = async(user)  => {
  try {
    const tablename = user.roleid == 1 ? "organiser" : "trekker";
    const username = user.roleid == 1 ? "organiser_name" : "trekkername";
    user.other_logindata = user.other_logindata ? JSON.stringify(user.other_logindata) : null;
    const stmt= `insert into ${tablename} (${username}, email, password, other_logindata, image) values (?,?,?,?,?)`;
    const data = [user.username, user.email, user.password, user.other_logindata, user.image]; 
    const result = await query(stmt, data);
    return await result;
  } catch (error) {
    throw error;
  }  
};

const getTrekkerByEmail = async (email) => {
  try {
    const stmt = `select * from trekker t where t.email = ?`; 
    const result = await query(stmt, [email]);
    return await result[0] ? result[0] : null;
  } catch (error) {
      throw error;
  }  
};

const getOrganiserByEmail = async (email) => {
  try {
    const stmt = `select * from organiser t where t.email = ?`; 
    const result = await query(stmt, [email]);
    return await result[0] ? result[0] : null;
  } catch (error) {
      throw error;
  }  
};

const setLogin = async(user) => {
  try {
    user.other_logindata = user.other_logindata ? JSON.stringify(user.other_logindata) : null;
    const stmt=`insert into login_details (userid, username, email, password, other_logindata, logintypeid, roleid, date) values (?,?,?,?,?,?,?,?)`;
    const result = await query(stmt, [user.userid, user.username, user.email, user.password, user.other_logindata, user.logintypeid, user.roleid, user.date]);
    return await result;
  } catch (error) {
    throw error;
  }  
};

module.exports = {
  addNewUser,
  getOrganiserByEmail,
  getTrekkerByEmail,
  setLogin,
};