const query = require("../database");

const updateProfileModel = async(profile)  => {
  try {
    const stmt = profile.roleid == 1
    ? `update organiser set organiser_name='${profile.organiser_name}', email='${profile.email}', password='${profile.password}',
    image='${profile.image}', mobile=${profile.mobile}, alternate_number=${profile.alternate_number}, address='${profile.address}',
    pincode='${profile.pincode}', city='${profile.city}',state='${profile.state}', country='${profile.country}' where organiserid=${profile.organiserid}`
    : `update trekker set trekkername='${profile.trekkername}', email='${profile.email}', password='${profile.password}',
    image='${profile.image}', mobile=${profile.mobile}, alternate_number=${profile.alternate_number}, address='${profile.address}',
    pincode='${profile.pincode}', city='${profile.city}',state='${profile.state}', country='${profile.country}' where trekkerid=${profile.trekkerid}`;
    const result = await query(stmt);
    return await result;
  } catch (error) {
    throw error;
  }  
};

const getProfile = async (profile) => {
  try {
    const stmt = profile.trekkerid
      ? `select * from trekker t where t.trekkerid = ${profile.trekkerid}`
      : `select * from organiser t where t.organiserid = ${profile.organiserid}`; 
    const result = await query(stmt);
    return await result[0] ? result[0] : null;
  } catch (error) {
    throw error;
  }  
};

module.exports = {
  getProfile,
  updateProfileModel,
};