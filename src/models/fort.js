
const query = require("../database");

const getAllForts = async(value)  => {
    try {
        const stmt = value
            ? `select *, (active + 0) as activestatus from fort where instr(fortname, '${value}') > 0 and active = 1`
            : `select *, (active + 0) as activestatus from fort where active = 1`;         
        const result = await query(stmt);
        return await result;
    } catch (error) {
        throw error;
    }  
};

module.exports = {
    getAllForts,
};