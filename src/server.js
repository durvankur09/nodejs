require("dotenv").config();
const exp = require("express");
const passport = require('passport');
const bodyParser = require("body-parser");
const allroutes = require("./routes/index");
const middleware = require("./middleware");
const ssorouter = require("./ssologin");

const app = exp();
const port = process.env.PORT || 8000;

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(middleware.cors);

app.use(passport.initialize()); // Used to initialize passport

app.use(passport.session()); // Used to persist login sessions

//ssologin router this should called before other router
app.use(ssorouter);

//middleware this should called before other routers
app.use(middleware.request_authetication);

//other routers
allroutes.getAllRoutes(app);

app.listen(port);
console.log('server started on: ' + port);