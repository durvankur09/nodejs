const exp = require("express");
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const OAuth2Data = require('./google_key.json');
const { addNewUser, getOrganiserByEmail, getTrekkerByEmail, setLogin } = require("./models/login");
const { sendResponse, sendError, encodeToken, getDateFormatToStore } = require("./utility/index");
const messages = require("./constants/sendmessages");
const { moment } = require("./utility/librarydata");

const CLIENT_ID = OAuth2Data.client.client_id;
const CLIENT_SECRET = OAuth2Data.client.client_secret;
const REDIRECT_URL = OAuth2Data.client.redirect_uris[0];

const ssoRouter = exp.Router();


// seeting og google login data
passport.use(new GoogleStrategy({
  clientID: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  callbackURL: REDIRECT_URL
}, (accessToken, refreshToken, profile, done) => {
    done(null, profile);
  })
);

// Used to stuff a piece of information into a cookie
passport.serializeUser((user, done) => {
  done(null, user);
});

// Used to decode the received cookie and persist session
passport.deserializeUser((user, done) => {
  done(null, user);
});


// starting url where  you can select your google account
ssoRouter.get('/login/sso', passport.authenticate('google', {
  scope: ['profile', 'email'] // Used to specify the required data
}));

// The middleware receives the data from Google and runs the function on Strategy config
// this redirect url gives us a serialise user object in request and check if gmail exist in our record then do login otherwise add new user
ssoRouter.get('/login/google', passport.authenticate('google'), async(req, response) => {
  try {  
    const profile = req.user;
    const user = {
      roleid: 1,
      image: profile._json.picture || null,
      email: profile._json.email || null,
      username: profile._json.name || null,
      password: null,
      other_logindata: {
        googleid: profile.id,
      },
    };
    const organiser = await getOrganiserByEmail(user.email);
    const trekker = await getTrekkerByEmail(user.email);
    const result = organiser || trekker;
    const loginobject = {
      userid: result ? result.organiserid || result.trekkerid : null,
      username: result ? result.organiser_name || result.trekkername : user.username,
      password: result ? result.password : null,
      email: result ? result.email : user.email,
      roleid: result ? result.organiserid ? 1 : 2 : user.roleid,
      other_logindata: result ? result.other_logindata : user.other_logindata,
      logintypeid: 1,
      date: getDateFormatToStore(),
    };

    // if result exist means user is already registered
    if (result) {
      const loginsuccess = await setLogin({ ...loginobject });
      if (loginsuccess) {
        const tokenobject = {
          roleid: result.organiserid ? 1 : 2,
          userid: loginobject.userid,
          username: loginobject.username,
          email: loginobject.email,
          image: user.image,
          expirytime: Math.floor(Date.now() / 1000) + parseInt(process.env.JWT_EXPIRY_TIME),
        };
        tokenobject.token = encodeToken({ ...tokenobject });
        return sendResponse(response, tokenobject);
      } else {
        return sendError(response, err.sqlMessage || messages.login_fail);
      }
    } else {
      // if result is not exist means user is not registered
      const addusersuccess = await addNewUser({ ...user });
      if (addusersuccess) {
        const loginsuccess = await setLogin({ ...loginobject, userid: addusersuccess.insertId });
        if (loginsuccess) {
          const tokenobject = {
            roleid: 1,
            userid: addusersuccess.insertId,
            username: loginobject.username,
            email: loginobject.email,
            image: user.image,
            expirytime: Math.floor(Date.now() / 1000) + parseInt(process.env.JWT_EXPIRY_TIME),
          };
          tokenobject.token = encodeToken({ ...tokenobject });
          return sendResponse(response, tokenobject);
        } else {
          return sendError(response, err.sqlMessage || messages.login_fail);
        }
      } else {
        return sendError(response, err.sqlMessage || messages.login_fail);
      }
    }
  } catch (err) {
    return sendError(response, err.sqlMessage || messages.login_fail);
  }
});

module.exports = ssoRouter;
