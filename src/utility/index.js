const { bcrypt, jwt, moment } = require("./librarydata");
require("dotenv").config();
const { genSaltSync, hashSync, compareSync } = bcrypt;


const sendResponse = (response, result) => {
	if (result.iserror) {
		return response.status(500).json({
			error: {
				msg: result.msg,
			},
			result: null,
		});
	} else {
		return response.status(200).json({
			error: null,
			result: [
					{
							data: result.result,
					}
			]
		});
	}
};

const sendError = (response, msg) => {
	const data = {
		msg,
	};
	return response.status(500).json({ error: data, result: null });
}

const sendResult = (error, result, msg) => {
	return {
		iserror: error,
		result,
		msg,
	};
}

const getEncryptedData = (data) => {
	const salt = genSaltSync(10);
	return hashSync(data, salt);
}

const compareEncrytedData = (data1, data2) => {
	return compareSync(data1, data2);
}

const encodeToken = (data) => {
	console.log('process.env.JWT_EXPIRY_TIME: ', process.env.JWT_EXPIRY_TIME);
	return jwt.sign(data, process.env.JWT_PRIVATE_KEY, { expiresIn: parseInt(process.env.JWT_EXPIRY_TIME) });
}

const decodeToken = (data) => {
	return jwt.decode(data, process.env.JWT_PRIVATE_KEY);
}

const verifyToken = (data) => {
	return jwt.verify(data, process.env.JWT_PRIVATE_KEY);
}

const getDateFormatToStore = (date) => {
	if (date && moment(date, "yyyy-mm-dd HH:mm:ss", true).isValid()) {
		return date;
	} else if (date && !moment(date, "yyyy-mm-dd HH:mm:ss", true).isValid()) {
		return "";
	} else if (!date) {
		return moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
	}
}

module.exports = {
	sendResponse,
	sendError,
	sendResult,
	getEncryptedData,
	compareEncrytedData,
	encodeToken,
	decodeToken,
	verifyToken,
	getDateFormatToStore,
}