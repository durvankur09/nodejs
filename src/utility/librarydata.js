module.exports = {
    fs: require('fs'),
    path: require("path"),
    moment: require('moment'),
    bcrypt: require("bcrypt"),
    jwt: require("jsonwebtoken"),
}