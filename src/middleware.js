const { verifyToken, sendError, sendResult, sendResponse } = require("./utility/index");
const messages = require("./constants/sendmessages");
module.exports = {
  request_authetication: (req, res, next) => {
    if (req.method.toUpperCase() == "OPTIONS" || (req.headers && req.headers.authorization == "Global")) {
      next();
    } else if (req.headers && req.headers.authorization != null) {
      try {
        //checking token is valid or not if valid then forwording else throwing error
        const obj = verifyToken(req.headers.authorization);
        if (Math.floor(Date.now() / 1000) > obj.exp) {
          return sendResponse(res, sendResult(true, null, messages.reverifying));
        } else {
          next();
        }
      } catch(err) {
        return sendError(res, err.message);
      }
    } else {
      return sendError(res, messages.token_invalid);
    }
  },
  cors: (req, res, next) => {
    // it enables http requests with body and headers
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
},
}